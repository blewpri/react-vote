import { eventChannel } from 'redux-saga';
import { User } from './authentication/authentication.model';

export const createFirebaseChannel = <T>(firebase: any, ref: string) => {
    return eventChannel((emit) => {
        const successHandler = <T>(payload: T) => emit(payload);
        const errorHandler = (error:any) => emit(new Error(error));
        const firebaseRef: any = firebase.database().ref(ref);
        const firebaseRefHandler: any = firebaseRef.on('value', 
            (data:T) => {
                successHandler<T>(data);
            },
            errorHandler({}));

        const unsubscribe = () => 
            firebaseRef.off('value', firebaseRefHandler);

        // the subscriber must return an unsubscribe function
        // this will be invoked when the saga calls `channel.close` method
        return unsubscribe
    });
}

export const createFirebaseAuthChannel = <T>(firebase: any) => {
    return eventChannel((emit) => {
        const successHandler = <T>(payload: T) => emit(payload);
        return firebase.auth().onAuthStateChanged(
            (authData: firebase.User | null) =>  {
                const result = { user: authData ? new User(authData.uid, authData.email) : null };
                successHandler(result);
            } 
        );
    });
}