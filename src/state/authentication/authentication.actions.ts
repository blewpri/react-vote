import * as types from './authentication.types';
import { User } from './authentication.model';

export const sendEmail = (email: string): types.AuthenticationActionTypes => {
    return {
        type: types.AUTHENTICATION_SEND_EMAIL,
        payload: { email: email }
    };
};

export const sendEmailSucceeded = (): types.AuthenticationActionTypes => {
    return {
        type: types.AUTHENTICATION_SEND_EMAIL_SUCCEEDED
    };
};

export const sendEmailFailed = (): types.AuthenticationActionTypes => {
    return {
        type: types.AUTHENTICATION_SEND_EMAIL_FAILED
    };
};

export const authenticationCheck = (): types.AuthenticationActionTypes => {
    return {
        type: types.AUTHENTICATION_CHECK
    };
};

export const authenticationCheckAuthenticated = (payload: User): types.AuthenticationActionTypes => {
    return {
        type: types.AUTHENTICATION_CHECK_AUTHENTICATED,
        payload: payload
    };
};

export const authenticationCheckUnathenticated = (): types.AuthenticationActionTypes => {
    return {
        type: types.AUTHENTICATION_CHECK_UNAUTHENTICATED
    };
};

export const authenticationComplete = (): types.AuthenticationActionTypes => {
    return {
        type: types.AUTHENTICATION_COMPLETE
    };
};

export const authenticationCompleteSucceeded = (): types.AuthenticationActionTypes => {
    return {
        type: types.AUTHENTICATION_COMPLETE_SUCCEEDED
    };
};

export const authenticationCompleteFailed = (): types.AuthenticationActionTypes => {
    return {
        type: types.AUTHENTICATION_COMPLETE_FAILED
    };
};