import { User } from './authentication.model';

export const AUTHENTICATION_SEND_EMAIL = 'AUTHENTICATION_SEND_EMAIL';
export const AUTHENTICATION_SEND_EMAIL_SUCCEEDED = 'AUTHENTICATION_SEND_EMAIL_SUCCEEDED';
export const AUTHENTICATION_SEND_EMAIL_FAILED = 'AUTHENTICATION_SEND_EMAIL_FAILED';
export const AUTHENTICATION_CHECK = 'AUTHENTICATION_CHECK';
export const AUTHENTICATION_CHECK_AUTHENTICATED = 'AUTHENTICATION_CHECK_AUTHENTICATED';
export const AUTHENTICATION_CHECK_UNAUTHENTICATED = 'AUTHENTICATION_CHECK_UNAUTHENTICATED';
export const AUTHENTICATION_COMPLETE = 'AUTHENTICATION_COMPLETE';
export const AUTHENTICATION_COMPLETE_SUCCEEDED = 'AUTHENTICATION_COMPLETE_SUCCEEDED';
export const AUTHENTICATION_COMPLETE_FAILED = 'AUTHENTICATION_COMPLETE_FAILED';

interface AuthenticationSendEmail {
    type: typeof AUTHENTICATION_SEND_EMAIL
    payload: { email: string }
}

interface AuthenticationSendEmailSucceeded {
    type: typeof AUTHENTICATION_SEND_EMAIL_SUCCEEDED
}

interface AuthenticationSendEmailFailed {
    type: typeof AUTHENTICATION_SEND_EMAIL_FAILED
}

interface AuthenticationCheck {
    type: typeof AUTHENTICATION_CHECK
}

interface AuthenticationCheckAuthenticated {
    type: typeof AUTHENTICATION_CHECK_AUTHENTICATED
    payload: User
}

interface AuthenticationCheckUnathenticated {
    type: typeof AUTHENTICATION_CHECK_UNAUTHENTICATED
}

interface AuthenticationComplete {
    type: typeof AUTHENTICATION_COMPLETE
}

interface AuthenticationCompleteSucceeded {
    type: typeof AUTHENTICATION_COMPLETE_SUCCEEDED
}

interface AuthenticationCompleteFailed {
    type: typeof AUTHENTICATION_COMPLETE_FAILED
}

export type AuthenticationActionTypes = 
    AuthenticationSendEmail | AuthenticationSendEmailSucceeded | AuthenticationSendEmailFailed |
    AuthenticationCheck | AuthenticationCheckAuthenticated | AuthenticationCheckUnathenticated |
    AuthenticationComplete | AuthenticationCompleteSucceeded | AuthenticationCompleteFailed;