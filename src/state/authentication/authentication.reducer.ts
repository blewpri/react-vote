import { AuthenticationState } from './authentication.model';
import * as types from './authentication.types';

export const authenticationReducer = (state = new AuthenticationState(), action: types.AuthenticationActionTypes): AuthenticationState => {
    switch (action.type) {
        case types.AUTHENTICATION_SEND_EMAIL:
            return {
                ...state,
                isBusy: true,
                email: action.payload.email
            };
        case types.AUTHENTICATION_SEND_EMAIL_SUCCEEDED:
            return {
                ...state,
                isBusy: false
            };
        case types.AUTHENTICATION_SEND_EMAIL_FAILED:
            return {
                ...state,
                isBusy: false
            };
        case types.AUTHENTICATION_CHECK:
            return {
                ...state,
                isBusy: true,
            }
        case types.AUTHENTICATION_CHECK_AUTHENTICATED:
            return {
                ...state,
                uid: action.payload.uid,
                email: action.payload.email || '',
                isBusy: false,
                isInitialized: true,
                isAuthenticated: true
            };
        case types.AUTHENTICATION_CHECK_UNAUTHENTICATED:
            return {
                ...state,
                uid: '',
                email: '',
                isBusy: false,
                isInitialized: true,
                isAuthenticated: false,
            };
        default:
            return state;
    }
};