import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const signInEmailKey = 'emailForSignIn';

export const sendAuthenticationEmail = (email: string): Promise<any> => {
    email = email.trim();
    const actionCodeSettings = {
        // URL you want to redirect back to. The domain (www.example.com) for this
        // URL must be whitelisted in the Firebase Console.
        url: 'http://localhost:3000/sign-in',
        handleCodeInApp: true,
    };
    return firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings)
        .then(() => {
            // The link was successfully sent. Inform the user.
            // Save the email locally so you don't need to ask the user for it again
            // if they open the link on the same device.
            window.localStorage.setItem(signInEmailKey, email);
        });

};

export const completeAuth = (): Promise<any> => {
    return new Promise<any>((resolve, reject) => {
        // Confirm the link is a sign-in with email link.
        if (firebase.auth().isSignInWithEmailLink(window.location.href)) {
            // Additional state parameters can also be passed via URL.
            // This can be used to continue the user's intended action before triggering
            // the sign-in operation.
            // Get the email if available. This should be available if the user completes
            // the flow on the same device where they started it.
            var email = window.localStorage.getItem(signInEmailKey);
            if (!email) {
                // User opened the link on a different device. To prevent session fixation
                // attacks, ask the user to provide the associated email again. For example:
                email = window.prompt('Please provide your email for confirmation');
            }
            email = email || '';
            // The client SDK will parse the code from the link for you.
            firebase.auth().signInWithEmailLink(email, window.location.href)
                .then((result: any) => {
                    // Clear email from storage.
                    window.localStorage.removeItem(signInEmailKey);
                    // You can access the new user via result.user
                    // Additional user info profile not available via:
                    // result.additionalUserInfo.profile == null
                    // You can check if the user is new or existing:
                    // result.additionalUserInfo.isNewUser
                    resolve();
                })
                .catch((error: any) => {
                    // Some error occurred, you can inspect the code: error.code
                    // Common errors could be invalid email and invalid or expired OTPs.
                    reject();
                });
        } else {
            reject();
        }
    });
}