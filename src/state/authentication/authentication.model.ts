export class AuthenticationState {
    
    constructor(
        public email: string = '',
        public uid: string = '',
        public isAuthenticated: boolean = false,
        public isBusy: boolean = false,
        public isInitialized: boolean = false
    ) {}

}

export class User {
    constructor( 
        public uid: string = '',
        public email: string|null = ''
    ){}
}