import { all, select, take, put, call } from 'redux-saga/effects';
import * as firebase from 'firebase/app';
import 'firebase/database';
import * as types from './authentication.types';
import * as actions from './authentication.actions';
import { IAppState } from '../app.model';
import { createFirebaseAuthChannel } from '../app.firebase-utils';
import * as service from './authentication.service';

function* authenticationSendEmail() {
    try {
        yield take(types.AUTHENTICATION_SEND_EMAIL);
        const email = yield select((state: IAppState) => state.authentication.email);
        yield service.sendAuthenticationEmail(email);
        yield put(actions.sendEmailSucceeded());
    } catch(ex) {
        yield put(actions.sendEmailFailed());
    }
}

function* authenticationComplete() {
    try {
        yield take(types.AUTHENTICATION_COMPLETE);
        yield service.completeAuth();
        yield put(actions.authenticationCompleteSucceeded());
    } catch(ex) {
        yield put(actions.authenticationCompleteFailed());
    }
}

function* authenticationWatch() {
    const accountsChannel = yield call(createFirebaseAuthChannel, firebase);
    while(true) {
        try {
            const payload = yield take(accountsChannel);
            const action = payload.user ? 
                actions.authenticationCheckAuthenticated(payload.user) :
                actions.authenticationCheckUnathenticated();
            yield put(action);
        } catch(err) {
            console.log('Marts brand new error');
        }
    }
}

export default function* authSagas() {
    yield all([
        authenticationComplete(),
        authenticationWatch(),
        authenticationSendEmail()
    ])
}

