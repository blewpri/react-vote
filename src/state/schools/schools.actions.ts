import * as types from './schools.types';
import { ILocation } from './schools.model';

export const getSchoolsLocationApi = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOLS_LOCATION_API_GET
    };
};

export const getSchoolsLocationApiSucceeded = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOLS_LOCATION_API_GET_SUCCEEDED
    };
};

export const getSchoolsLocationApiFailed = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOLS_LOCATION_API_GET_FAILED
    };
};

export const getCurrentLocation = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOLS_CURRENT_LOCATION_GET
    };
};

export const getCurrentLocationSucceeded = (payload: ILocation): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOLS_CURRENT_LOCATION_GET_SUCCEEDED,
        payload: payload
    };
};

export const getCurrentLocationFailed = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOLS_CURRENT_LOCATION_GET_FAILED
    };
};

export const getNearestSchools = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOLS_GET_NEAREST
    };
};

export const getNearestSchoolsSucceeded = (payload: Array<ILocation>): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOLS_GET_NEAREST_SUCCEEDED,
        payload: payload
    };
};

export const getNearestSchoolsFailed = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOLS_GET_NEAREST_FAILED
    };
};

export const getSchool = (id: string): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOL_GET,
        payload: id
    };
};

export const getSchoolSucceeded = (payload: ILocation): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOL_GET_SUCCEEDED,
        payload: payload
    };
};

export const getSchoolFailed = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOL_GET_FAILED
    };
};

export const addSchool = (payload: ILocation): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOL_ADD,
        payload: payload
    };
};

export const addSchoolSucceeded = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOL_ADD_SUCCEEDED
    };
};

export const addSchoolFailed = (): types.SchoolsActionTypes => {
    return {
        type: types.SCHOOL_ADD_FAILED
    };
};

