import { SchoolsState, ILocation } from './schools.model';
import * as types from './schools.types';

export const schoolsReducer = (state = new SchoolsState(), action: types.SchoolsActionTypes): SchoolsState => {
    switch (action.type) {
        case types.SCHOOLS_LOCATION_API_GET_SUCCEEDED:
            return {
                ...state,
                hasDownloadedApi: true
            };
        case types.SCHOOLS_CURRENT_LOCATION_GET_SUCCEEDED:
            return {
                ...state,
                hasCurrentLocation: true,
                currentLocation: action.payload                
            };
        case types.SCHOOLS_GET_NEAREST:
            return {
                ...state,
                isBusy: true,
            }
        case types.SCHOOLS_GET_NEAREST_SUCCEEDED:
            return {
                ...state,
                isBusy: false,
                locations: {
                    ...state.locations,
                    ...action.payload.reduce((obj: { [id:string]: ILocation }, location) => {
                        if(location.id) obj[location.id] = location;
                        return obj;
                    }, {}),
                },
                nearest: [
                    ...action.payload.map(item => item.id || '') || []
                ]
            };
        case types.SCHOOL_ADD:
            return {
                ...state,
                locations : {
                    ...state.locations,
                    [action.payload.id]: action.payload 
                }
            }
        default:
            return state;
    }
};