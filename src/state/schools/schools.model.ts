export class SchoolsState {
    public locations: { [id: string]: ILocation } = {};
    public currentLocation?: ILocation;
    public nearest: Array<string> = [];
    constructor(
        public hasDownloadedApi: boolean = false,
        public hasCurrentLocation: boolean = false,
        public isBusy: boolean = false
    ) {}
}

export interface ILocation {
    name: string,
    description?: string,
    id: string;
    lat?: number;
    lng?: number; 

}