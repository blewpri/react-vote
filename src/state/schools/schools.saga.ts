import { all, call, put, select, take } from 'redux-saga/effects';
import { IAppState } from '../app.model';
import * as actions from './schools.actions';
import * as types from './schools.types';
import * as service from './schools.service';

export function* getNearestSchools() {
    try {
        yield take(types.SCHOOLS_GET_NEAREST);
        yield all([
            getLocationApi(),
            getCurrent()
        ]);
        const location = yield select((state: IAppState) => state.schools.currentLocation);
        const schools = yield service.getNearestPrimarySchools(location);
        yield put(actions.getNearestSchoolsSucceeded(schools));
    } catch (e) {
        yield put(actions.getNearestSchoolsFailed());  
    } 
}

export function* getLocationApi() {  
    try {
        yield put(actions.getSchoolsLocationApi());
        yield call(service.getSchoolsLocationsApiScript);
        yield put(actions.getSchoolsLocationApiSucceeded());
    } catch (e) {
        yield put(actions.getSchoolsLocationApiFailed());
    }
}

export function* getCurrent() {
    try {
        yield put(actions.getCurrentLocation());
        let location = yield call(service.getCurrentLocation);
        yield put(actions.getCurrentLocationSucceeded(location));
    } catch (e) {
        yield put(actions.getCurrentLocationFailed());
    }
}

export function* getSchool() {
    try {
        const action = yield take(types.SCHOOL_GET);
        const school = yield service.getSchool(action.payload);
        yield put(actions.getSchoolSucceeded(school));
    } catch (e) {
        yield put(actions.getSchoolFailed());  
    }
}

export function* addSchool() {
    try {
        const action = yield take(types.SCHOOL_ADD);
        yield service.addSchool(action.payload);
        yield put(actions.addSchoolSucceeded());
    } catch (e) {
        yield put(actions.addSchoolFailed());  
    }
}

export default function* schoolsSagas() {
    yield all([
        getNearestSchools(),
        getSchool(),
        addSchool()
    ])
}

