import { ILocation } from './schools.model';
export const SCHOOLS_LOCATION_API_GET = 'SCHOOLS_LOCATION_API_GET';
export const SCHOOLS_LOCATION_API_GET_SUCCEEDED = 'SCHOOLS_LOCATION_API_GET_SUCCEEDED';
export const SCHOOLS_LOCATION_API_GET_FAILED = 'SCHOOLS_LOCATION_API_GET_FAILED';
export const SCHOOLS_CURRENT_LOCATION_GET = 'SCHOOLS_CURRENT_LOCATION_GET';
export const SCHOOLS_CURRENT_LOCATION_GET_SUCCEEDED = 'SCHOOLS_CURRENT_LOCATION_GET_SUCCEEDED';
export const SCHOOLS_CURRENT_LOCATION_GET_FAILED = 'SCHOOLS_CURRENT_LOCATION_GET_FAILED';
export const SCHOOLS_GET_NEAREST = 'SCHOOLS_GET_NEAREST';
export const SCHOOLS_GET_NEAREST_SUCCEEDED = 'SCHOOLS_GET_NEAREST_SUCCEEDED';
export const SCHOOLS_GET_NEAREST_FAILED = 'SCHOOLS_GET_NEAREST_FAILED';
export const SCHOOL_GET = 'SCHOOL_GET';
export const SCHOOL_GET_SUCCEEDED = 'SCHOOL_GET_SUCCEEDED';
export const SCHOOL_GET_FAILED = 'SCHOOL_GET_FAILED';
export const SCHOOL_ADD = 'SCHOOL_ADD';
export const SCHOOL_ADD_SUCCEEDED = 'SCHOOL_ADD_SUCCEEDED';
export const SCHOOL_ADD_FAILED = 'SCHOOL_ADD_FAILED';

interface SchoolsLocationApiGet {
    type: typeof SCHOOLS_LOCATION_API_GET
}

interface SchoolsLocationApiGetSucceeded {
    type: typeof SCHOOLS_LOCATION_API_GET_SUCCEEDED
}

interface SchoolsLocationApiGetFailed {
    type: typeof SCHOOLS_LOCATION_API_GET_FAILED
}

interface SchoolsCurrentLocationGet {
    type: typeof SCHOOLS_CURRENT_LOCATION_GET
}

interface SchoolsCurrentLocationGetSucceeded {
    type: typeof SCHOOLS_CURRENT_LOCATION_GET_SUCCEEDED
    payload: ILocation
}

interface SchoolsCurrentLocationGetFailed {
    type: typeof SCHOOLS_CURRENT_LOCATION_GET_FAILED
}

interface SchoolsGetNearest {
    type: typeof SCHOOLS_GET_NEAREST
}

interface SchoolsGetNearestSucceeded {
    type: typeof SCHOOLS_GET_NEAREST_SUCCEEDED
    payload: Array<ILocation>
}

interface SchoolsGetNearestFailed {
    type: typeof SCHOOLS_GET_NEAREST_FAILED
}

interface SchoolGet {
    type: typeof SCHOOL_GET
    payload: string
}

interface SchoolGetSucceeded {
    type: typeof SCHOOL_GET_SUCCEEDED
    payload: ILocation
}

interface SchoolGetFailed {
    type: typeof SCHOOL_GET_FAILED
}

interface SchoolAdd {
    type: typeof SCHOOL_ADD,
    payload: ILocation
}

interface SchoolAddSucceeded {
    type: typeof SCHOOL_ADD_SUCCEEDED
    
}

interface SchoolAddFailed {
    type: typeof SCHOOL_ADD_FAILED
}

export type SchoolsActionTypes =
    SchoolsLocationApiGet | SchoolsLocationApiGetSucceeded | SchoolsLocationApiGetFailed |
    SchoolsCurrentLocationGet | SchoolsCurrentLocationGetSucceeded | SchoolsCurrentLocationGetFailed |
    SchoolsGetNearest | SchoolsGetNearestSucceeded | SchoolsGetNearestFailed |
    SchoolGet | SchoolGetSucceeded | SchoolGetFailed |
    SchoolAdd | SchoolAddSucceeded | SchoolAddFailed;
    