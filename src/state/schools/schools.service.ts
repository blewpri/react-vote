import { ILocation } from './schools.model';
import { config } from 'environment';
import * as firebase from 'firebase/app';
import 'firebase/database';

export const getSchoolsLocationsApiScript = (): Promise<any> => {
    return new Promise((resolve, reject) => {
        const ApiKey = config.googleMaps.apiKey;
        const script: HTMLScriptElement = document.createElement('script');
        script.async = true;
        script.src = `https://maps.googleapis.com/maps/api/js?key=${ApiKey}&libraries=places`;
        script.onload = () => resolve();
        script.onerror = () => reject();
        document.body.appendChild(script);
    });
};

export const getCurrentLocation = (): Promise<any> => {
    return new Promise<ILocation>((resolve, reject) => {
        navigator
            .geolocation
            .getCurrentPosition(
                success => {
                    resolve({
                        id: '',
                        name: 'Your Location',
                        lat: success.coords.latitude,
                        lng: success.coords.longitude
                    });
                },
                failed => {
                    // TODO: Implement
                }
            )
    });
}; 

export const getNearestPrimarySchools = (location: ILocation): Promise<Array<ILocation>> => {
    return new Promise<Array<ILocation>>((resolve, reject) => { 

        let service = new google
            .maps.places
            .PlacesService(document.createElement('div'));

        service.nearbySearch({
            location: {
                lat: location.lat || 0,
                lng: location.lng || 0
            },
            rankBy: google.maps.places.RankBy.DISTANCE,
            type: 'primary_school'
        }, (results) => {
            let res: Array<ILocation> = results.map(result => {
                return { 
                    id: result.id || '', 
                    name: result.name,
                    description: result.vicinity,
                    lat: result.geometry ? result.geometry.location.lat() : undefined,
                    lng: result.geometry ? result.geometry.location.lng() : undefined
                 };
            });
            if (res.length > 3) {
                res = res.slice(0,3);
            }
            return resolve(res);
        });

    });
    
};

export const getSchool = (schoolId: string): Promise<any> => {
    return firebase.database()
        .ref(`/schools/${schoolId}`)
        .once('value')
        .then((snapshot) => snapshot ? snapshot.val() : null);
};

export const addSchool = (school: ILocation): Promise<any> => {
    return firebase.database()
        .ref(`/schools/${school.id}`)
        .set(school);
};