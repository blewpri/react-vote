import { ILocation } from "state/schools/schools.model";

export const ACCOUNT_SCHOOL_GET = 'ACCOUNT_SCHOOL_GET';
export const ACCOUNT_SCHOOL_GET_SUCCEEDED = 'ACCOUNT_SCHOOL_GET_SUCCEEDED';
export const ACCOUNT_SCHOOL_GET_FAILED = 'ACCOUNT_SCHOOL_GET_FAILED';
export const ACCOUNT_SCHOOL_SET = 'ACCOUNT_SCHOOL_SET';
export const ACCOUNT_SCHOOL_SET_SUCCEEDED = 'ACCOUNT_SCHOOL_SET_SUCCEEDED';
export const ACCOUNT_SCHOOL_SET_FAILED = 'ACCOUNT_SCHOOL_SET_FAILED';

interface AccountSchoolGet {
    type: typeof ACCOUNT_SCHOOL_GET
}

interface AccountSchoolGetSucceeded {
    type: typeof ACCOUNT_SCHOOL_GET_SUCCEEDED,
    payload: string|null
}

interface AccountSchoolGetFailed {
    type: typeof ACCOUNT_SCHOOL_GET_FAILED
}

interface AccountSchoolSet {
    type: typeof ACCOUNT_SCHOOL_SET
    payload: ILocation;
}

interface AccountSchoolSetSucceeded {
    type: typeof ACCOUNT_SCHOOL_SET_SUCCEEDED
}

interface AccountSchoolSetFailed {
    type: typeof ACCOUNT_SCHOOL_SET_FAILED
}

export type AccountActionTypes = 
    AccountSchoolGet | AccountSchoolGetSucceeded | AccountSchoolGetFailed |
    AccountSchoolSet | AccountSchoolSetSucceeded | AccountSchoolSetFailed;