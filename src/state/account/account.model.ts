export class AccountState {
    public schoolId?: string;
    public isBusy: boolean = false;
    public isUpdating: boolean = false;
}  