import * as firebase from 'firebase/app';
import 'firebase/database';
import { ILocation } from '../schools/schools.model';

export const getSchool = (uid: string): Promise<any> => {
    return firebase.database().ref(`/accounts/${uid}/schoolId`)
        .once('value')
        .then((snapshot) => snapshot ? snapshot.val() : null);
};

export const setSchool = (uid: string, location: ILocation): Promise<any> => {
    return firebase.database().ref(`/accounts/${uid}/schoolId`)
        .set(location.id);
};

