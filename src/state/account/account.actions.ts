import * as types from './account.types';
import { ILocation } from '../schools/schools.model';

export const accountSchoolGet = (): types.AccountActionTypes => {
    return {
        type: types.ACCOUNT_SCHOOL_GET
    };
};

export const accountSchoolGetSucceeded = (payload: string): types.AccountActionTypes => {
    return {
        type: types.ACCOUNT_SCHOOL_GET_SUCCEEDED,
        payload: payload
    };
};

export const accountSchoolGetFailed = (): types.AccountActionTypes => {
    return {
        type: types.ACCOUNT_SCHOOL_GET_FAILED
    };
};

export const accountSchoolSet = (payload: ILocation): types.AccountActionTypes => {
    return {
        type: types.ACCOUNT_SCHOOL_SET,
        payload: payload  
    };
};

export const accountSchoolSetSucceeded = (): types.AccountActionTypes => {
    return {
        type: types.ACCOUNT_SCHOOL_SET_SUCCEEDED
    };
};

export const accountSchoolSetFailed = (): types.AccountActionTypes => {
    return {
        type: types.ACCOUNT_SCHOOL_SET_FAILED
    };
};
