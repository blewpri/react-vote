import { all, select, take, put } from 'redux-saga/effects';
import * as authTypes from '../authentication/authentication.types';
import * as actions from './account.actions';
import * as types from './account.types';
import * as service from './account.service';
import * as schoolActions from '../schools/schools.actions';
import { IAppState } from '../app.model';

function* accountSchool() {
    try {
        yield take(authTypes.AUTHENTICATION_CHECK_AUTHENTICATED);
        yield put(actions.accountSchoolGet());
        const uid = yield select((state: IAppState) => state.authentication.uid);
        const schoolId = yield service.getSchool(uid);
        yield put(actions.accountSchoolGetSucceeded(schoolId));
        (schoolId) ?
            yield put(schoolActions.getSchool(schoolId)) :
            yield put(schoolActions.getNearestSchools());
    } catch(ex) {
        yield put(actions.accountSchoolGetFailed());
    }
}

function* setSchool() { 
    try {
        const action: any  = yield take(types.ACCOUNT_SCHOOL_SET);
        const uid = yield select((state: IAppState) => state.authentication.uid);
        yield service.setSchool(uid, action.payload);
        yield put(schoolActions.addSchool(action.payload));
        yield put(actions.accountSchoolSetSucceeded())
    } catch(ex) {
        yield put(actions.accountSchoolSetFailed())
    }
}

export default function* accountSagas() {
    yield all([
        accountSchool(),
        setSchool()
    ])
}

