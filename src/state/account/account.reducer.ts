import { AccountState } from './account.model';
import * as types from './account.types';

export const accountReducer = (state = new AccountState(), action: types.AccountActionTypes): AccountState => {
    switch (action.type) {
        case types.ACCOUNT_SCHOOL_GET:
            return {
                ...state,
                isBusy: true
            };
        case types.ACCOUNT_SCHOOL_GET_SUCCEEDED:
            return {
                ...state,
                isBusy: false,
                schoolId: action.payload || undefined
            };
        case types.ACCOUNT_SCHOOL_SET:
            return {
                ...state,
                isUpdating: true,
                schoolId: action.payload.id
            };
        case types.ACCOUNT_SCHOOL_SET_SUCCEEDED:
            return {
                ...state,
                isUpdating: false,
            };
        default:
            return state;
    }
};