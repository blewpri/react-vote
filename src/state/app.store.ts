import { combineReducers, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import authSagas from './authentication/authentication.saga';
import accountSagas from './account/account.saga';
import { accountReducer } from './account/account.reducer';
import { authenticationReducer } from './authentication/authentication.reducer';
import { schoolsReducer } from './schools/schools.reducer';
import schoolsSagas from './schools/schools.saga';
import { IAppState } from './app.model';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import './app.firebase';

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
    combineReducers<IAppState>({ 
        account: accountReducer,
        authentication: authenticationReducer,
        schools: schoolsReducer
    }),
    composeWithDevTools(applyMiddleware(sagaMiddleware)),
); 

sagaMiddleware.run(authSagas);
sagaMiddleware.run(accountSagas); 
sagaMiddleware.run(schoolsSagas); 

