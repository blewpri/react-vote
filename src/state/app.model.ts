import { AccountState } from './account/account.model';
import { SchoolsState } from './schools/schools.model';
import { AuthenticationState } from './authentication/authentication.model';

export interface IAppState {
    account: AccountState,
    authentication: AuthenticationState,
    schools: SchoolsState
}