import { config } from 'environment';
import * as firebase from 'firebase/app';
import 'firebase/analytics';
import 'firebase/functions';

firebase.initializeApp(config.firebase);
firebase.analytics();
firebase.functions();