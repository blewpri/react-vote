import React from 'react';
import {
	BrowserRouter as Router,
 	Switch
} from 'react-router-dom';
import { AppRoute } from 'components';
import { SecureContainer, AuthCompleteContainer, AuthEmailSentContainer, AuthContainer } from 'containers';
import { useSelector } from 'react-redux';
import { IAppState } from 'state';
// import { useAuthStatus } from 'hooks';
// import styles from './App.module.scss';

export interface AppProps {
	title: string
}

export const App: React.FC<AppProps> = (props) => { 
	const isAuthenticated = useSelector((state: IAppState) => state.authentication.isAuthenticated);
	const isInitialized = useSelector((state: IAppState) => state.authentication.isInitialized);
	return (
		<Router>
			<Switch>
				<AppRoute
					isBusy={!isInitialized}
					doRedirect={isAuthenticated}
					redirectPath="/"
					path="/sign-in" >
					<AuthCompleteContainer />
				</AppRoute>
				<AppRoute 
					isBusy={!isInitialized}
					doRedirect={isAuthenticated}
					redirectPath="/"
					path="/auth" >
					<AuthContainer />
				</AppRoute>
				<AppRoute
					isBusy={!isInitialized}
					doRedirect={isAuthenticated}
					redirectPath="/"
					path="/auth-email-sent" >
					<AuthEmailSentContainer />
				</AppRoute>
				<AppRoute 
					isBusy={!isInitialized}
					doRedirect={!isAuthenticated}
					redirectPath="/auth"
					path="/" >
					<SecureContainer />
				</AppRoute>
			</Switch>
		</Router>
	);
}

App.defaultProps = {
	title: 'Hello World'
};

export default App;
