import * as React from 'react';
import { useSelector } from 'react-redux';
import { IAppState, ILocation } from 'state';
import styles from './Map.module.scss';

export interface IMapProps {}

const defaultProps: IMapProps = {};

export const Map: React.FC<IMapProps> = (props: IMapProps) => {
    const nearest = useSelector((state: IAppState) => 
        state.schools.nearest.map((id: string) => 
            state.schools.locations[id]));
    
    const schools = (nearest || [])
        .map((value: ILocation, index: number) => {
            return (
                <div key={index}>{value.name}</div>
            );
        });

    return (
        <section className={styles.component}>
            {/* { JSON.stringify(position) } */}
            <div>{schools}</div>
        </section>
    );
}

Map.defaultProps = defaultProps;





