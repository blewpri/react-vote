import * as React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import styles from './ButtonProgress.module.scss';

export interface IButtonProgressProps {
    isBusy?: boolean;
    type?: 'submit' | 'button';
    onClick?: (event: any) => void
}

const defaultProps: IButtonProgressProps = {
    isBusy: false,
    type: 'button',
    onClick: (event: any) => {}  
};

export const ButtonProgress: React.FC<React.PropsWithChildren<IButtonProgressProps>> = (props) => {
    
    const onClick = (event: any) => {
        props.onClick && props.onClick(event); 
    }

    return (
        <Button
            className={styles.button}
            type="submit"
            variant="contained"
            size="medium"
            color="primary"
            onClick={ (e) => onClick(e) }>
            { props.isBusy ? (
                <CircularProgress className={styles.progress} size={23} />
            ) : (
                props.children
            )}
        </Button>  
    );
}

ButtonProgress.defaultProps = defaultProps;



