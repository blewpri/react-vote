import * as React from 'react';
import classNames from 'classnames';
import styles from './FullPage.module.scss';

export interface IFullPageProps {
    isCentered?: boolean;
}

const defaultProps: IFullPageProps = {
    isCentered: true
};

export const FullPage: React.FC<React.PropsWithChildren<IFullPageProps>> = (props) => {

    const componentClasses = classNames(
        styles.component, 
        { [styles.isCentered]: props.isCentered}
    );
    return (
        <section className={componentClasses}>
            {props.children}
        </section>
    );
}

FullPage.defaultProps = defaultProps;