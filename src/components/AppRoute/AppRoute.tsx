import * as React from 'react';
import { Route, Redirect, RouteProps } from 'react-router';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FullPage } from 'components';

export interface IAppRouteProps extends RouteProps {
    isBusy: boolean;
    doRedirect: boolean;
    redirectPath: string;
}

const defaultProps: IAppRouteProps = { 
    isBusy: false,
    doRedirect: false,
    redirectPath: '/'
};

export const AppRoute: React.FC<IAppRouteProps> = ({ children , ...props }) => {
    return (
        <Route {...props} render={() => {
            if (props.isBusy) {
                return (
                    <FullPage>
                        <CircularProgress />
                    </FullPage>);
            } else {
                return props.doRedirect ? 
                    (<Redirect to={{ pathname: props.redirectPath }}/>) : 
                    (children)
                }
            }
        } />
    );
};

AppRoute.defaultProps = defaultProps;