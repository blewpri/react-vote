import * as React from 'react';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';

import { ButtonProgress } from 'components';

export interface IAuthFormProps {
    isBusy?: boolean;
    onSubmit: (email: string) => void
}

const defaultProps: IAuthFormProps = {
    isBusy: false,
    onSubmit: (email: string) => {}
};

export const AuthForm: React.FC<IAuthFormProps> = (props: IAuthFormProps) => {
    
    const [ email, setEmail ] = React.useState<string>('');
    const [ isValid, setIsValid ] = React.useState<boolean>(true);

    const handleChange = (event: any) => {
        setEmail(event.target.value);
    };
    
    const handleSubmit = (event: any) => {
        let isFormValid = event.currentTarget.reportValidity();
        setIsValid(isFormValid);
        if (isFormValid) {
            props.onSubmit(email); 
        }
        event.preventDefault();
    }

    return (
        <form noValidate autoComplete="off" onSubmit={(e) => handleSubmit(e)}>

            <FormControl variant="outlined">
                <InputLabel htmlFor="email">Email Address</InputLabel>
                <OutlinedInput
                    id="email"
                    type="email"
                    disabled={props.isBusy}
                    error={!isValid}
                    required
                    placeholder="eg. helen@myschool.sch.uk"
                    value={email}
                    onChange={ (event) => handleChange(event) }
                    endAdornment={
                        <InputAdornment position="end">
                            <ButtonProgress
                                isBusy={props.isBusy}
                                type="submit">
                                Sign Up
                            </ButtonProgress>
                        </InputAdornment>
                    } labelWidth={105} />
            </FormControl>
        </form>
    );
}

AuthForm.defaultProps = defaultProps;



