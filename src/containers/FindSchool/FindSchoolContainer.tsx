import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Card from '@material-ui/core/Card';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

import { FullPage } from 'components';
import { IAppState, ILocation, accountSchoolSet } from 'state';

export interface IFindSchoolContainerProps {}

const defaultProps: IFindSchoolContainerProps = {};

export const FindSchoolContainer: React.FC<IFindSchoolContainerProps> = (props: IFindSchoolContainerProps) => {
    const dispatch = useDispatch();
    const nearest = useSelector((state: IAppState) => 
        state.schools.nearest.map((id: string) => 
            state.schools.locations[id]));

    const handleClick = (event:any, value: ILocation) => {
        dispatch(accountSchoolSet(value));
        event.preventDefault();
    };
    const schools = (nearest || [])
        .map((value: ILocation, index: number) => {
            return (
                <ListItem key={index} button
                    onClick={(event) => handleClick(event, value)}>
                    <ListItemAvatar>
                        <Avatar aria-label="school">
                            {value.name.charAt(0)}
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText 
                        primary={value.name} 
                        secondary={value.description} />
                </ListItem>
            );
        });

    return (
        <FullPage>
            <Card>
                <List>
                    <Typography gutterBottom variant="h5" component="h2">
                        Select Your School
                    </Typography>
                    {schools}
                </List>
            </Card>
        </FullPage>
    );
}

FindSchoolContainer.defaultProps = defaultProps;