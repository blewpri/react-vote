import * as React from 'react';
import { FullPage } from 'components';

export interface IHomeContainerProps {}

const defaultProps: IHomeContainerProps = {};

export const HomeContainer: React.FC<IHomeContainerProps> = (props: IHomeContainerProps) => {
    return (
        <FullPage>
            HOME CONTAINER
        </FullPage>
    );
}

HomeContainer.defaultProps = defaultProps;