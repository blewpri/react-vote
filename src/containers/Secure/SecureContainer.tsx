import React from 'react';
import {
	BrowserRouter as Router,
 	Switch
} from 'react-router-dom';
import { AppRoute } from 'components';
import { HomeContainer, FindSchoolContainer } from 'containers';
import { useSelector } from 'react-redux';
import { IAppState } from 'state';

export interface SecureContainerProps {}

export const SecureContainer: React.FC<SecureContainerProps> = (props) => { 
	const hasSchool = useSelector((state: IAppState) => 
        !!state.account.schoolId); 
    const isLoadingSchools = useSelector((state: IAppState) => 
        state.schools.isBusy ||
        state.account.isBusy);
	return (
		<Router>
			<Switch>
				<AppRoute 
					isBusy={false}
					doRedirect={!hasSchool}
					redirectPath="/find-school"
					exact path="/" >
					<HomeContainer />
				</AppRoute>
				<AppRoute
					isBusy={isLoadingSchools}
					doRedirect={hasSchool}
					redirectPath="/"
					path="/find-school">
					<FindSchoolContainer />
				</AppRoute>
			</Switch>
		</Router>
	);
}

SecureContainer.defaultProps = {};

export default SecureContainer;
