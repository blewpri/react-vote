import * as React from 'react';
import { Redirect } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { IAppState, sendEmail } from 'state';
import { AuthForm, FullPage } from 'components';

export interface IAuthContainerProps {}

const defaultProps: IAuthContainerProps = {};

export const AuthContainer: React.FC<IAuthContainerProps> = (props: IAuthContainerProps) => {
    const dispatch = useDispatch();
    const emailSent = useSelector((state: IAppState) => state.authentication.email && !state.authentication.isBusy);
    const isBusy = useSelector((state: IAppState) => state.authentication.isBusy)
    
    return emailSent ?
        (<Redirect to={{ pathname: "/auth-email-sent" }} />) :
        (
            <FullPage>
                <AuthForm 
                    isBusy={isBusy}
                    onSubmit={ (email: string) => 
                        dispatch(sendEmail(email))} />
            </FullPage>
        );
}

AuthContainer.defaultProps = defaultProps;
