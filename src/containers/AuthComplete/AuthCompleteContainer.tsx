import * as React from 'react';
import { useDispatch } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { authenticationComplete } from 'state';
import { FullPage } from 'components';

export interface IAuthCompleteContainerProps {}

const defaultProps: IAuthCompleteContainerProps = {};

export const AuthCompleteContainer: React.FC<IAuthCompleteContainerProps> = (props: IAuthCompleteContainerProps) => {
    const dispatch = useDispatch();
    dispatch(authenticationComplete());
    return (
        <FullPage>
            <CircularProgress />
        </FullPage>
    );
}

AuthCompleteContainer.defaultProps = defaultProps;




