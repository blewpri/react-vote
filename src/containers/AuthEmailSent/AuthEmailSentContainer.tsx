import * as React from 'react';
import { FullPage } from 'components';

export interface IAuthEmailSentContainerProps {}

const defaultProps: IAuthEmailSentContainerProps = {};

export const AuthEmailSentContainer: React.FC<IAuthEmailSentContainerProps> = (props: IAuthEmailSentContainerProps) => {
    return (
        <FullPage>
            Email Sent. Check your Inbox
        </FullPage>
    );
}

AuthEmailSentContainer.defaultProps = defaultProps;