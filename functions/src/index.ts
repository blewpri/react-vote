import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

exports.setUpAcccount = functions.auth.user().onCreate((user) => {
    const database = admin.database();
    return database
        .ref('votes/brexit')
        .once('value')
        .then((vote) => database
            .ref(`accounts/${user.uid}`)
            .set({ votes: { [vote.key || '']: vote.val() } }));
});
